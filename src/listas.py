from src.nodos import Nodo


# Definicion de la clase 'ListaLigada':
class ListaLigada:
    def __init__(self):
        self.cabeza = None

    # Agrega un elemento al final de la lista
    def agregar_elemento(self, valor):
        nuevo_nodo = Nodo(valor)  # Crear un nuevo nodo
        if self.cabeza is None:  # Si la lista está vacía, el nuevo nodo se convierte en la cabeza
            self.cabeza = nuevo_nodo
        else:
            nodo_final = self.cabeza
            while nodo_final.siguiente:  # Avanza hasta el último nodo
                nodo_final = nodo_final.siguiente

            nodo_final.siguiente = nuevo_nodo  # Enlazar el nuevo nodo al último nodo
        return

    def buscar_elemento(self, valor):
        nodo_actual = self.cabeza
        lugar = 0

        while nodo_actual:
            #Compara el valor a el valor que se busca.
            if nodo_actual.valor == valor:
                return True
            else:
                nodo_actual = nodo_actual.siguiente #Avanza a otro nodo.
            lugar += 1
        return False #Se regresa si no ecuentra el valor.

    def elimina_cabeza(self):
        if self.cabeza is not None:
            eliminar_nodo = self.cabeza
            self.cabeza = eliminar_nodo.siguiente
            eliminar_nodo.siguiente = None
            return eliminar_nodo.valor #Regresa al nodo eliminado
        return None #Si la lista no tiene nodos, no se elimina nada.

    def elimina_rabo(self):
        if self.cabeza is None:
            return
        if self.cabeza.siguiente is None:
            self.cabeza = None
            return
        nodo_actual = self.cabeza
        while nodo_actual.siguiente.siguiente is not None:  # Avanza hasta el penúltimo nodo
                nodo_actual = nodo_actual.siguiente
        nodo_actual.siguiente = None
        return

    def tamagno(self):
        contador = 0
        nodo_actual = self.cabeza #Empezamos con la cabeza de la lista
        while nodo_actual != None:
            nodo_actual = nodo_actual.siguiente #Avanza al siguiente nodo
            contador += 1
        return contador

    def copia(self):
        nueva_lista = ListaLigada()
        actual = self.cabeza
        while actual:
            nueva_lista.agregar_elemento(actual.valor)
            actual = actual.siguiente
        return nueva_lista

    def __str__(self):
        valores = []
        actual = self.cabeza
        while actual:
            valores.append(str(actual.valor))
            actual = actual.siguiente
        return " -> ".join(valores)
