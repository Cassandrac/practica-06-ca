# Practica 06
En esta practica, exploraremos el concepto de listas ligadas en el contexto
de la programaci ́on en Python. Las listas ligadas son estructuras de datos que
constan de nodos. Cada nodo contiene un valor y una referencia al siguiente
nodo en la lista. A lo largo de esta pr ́actica, los estudiantes aprenderan a crear,
manipular y utilizar listas ligadas en Python


## Indicaciones
- Haz un fork del [repositorio Practica06](https://gitlab.com/estructurasdatos1/practica06)
- Agrega a los instructores de laboratorio como mantenedores: [Edgar](https://gitlab.com/leonKj), [David](https://gitlab.com/davidalencia)
- Marca como completada en el classroom y como comentario agrega el link a tu repositorio.


## Probar tu codigo
- Instala pytest
```bash
pip install pytest
```

Finalmente en la raiz de tu directorio ejecuta el siguiente comando:
```bash
pytest tests
```

![](media/pytest.gif)

Tambien puedes usar el main para verificar tus avances
![](media/main.gif)
