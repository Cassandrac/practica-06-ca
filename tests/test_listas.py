# Importa el código que deseas prob
from src.listas import ListaLigada
from src.nodos import Nodo


def test_agregar_elemento():
    # Crea una instancia de ListaLigada
    lista = ListaLigada()

    # Agrega elementos y verifica si la cabeza se establece correctamente
    lista.agregar_elemento(1)
    assert lista.cabeza.valor == 1

    lista.agregar_elemento(2)
    assert lista.cabeza.siguiente.valor == 2

    # Agrega más elementos y verifica que la lista se construya correctamente
    lista.agregar_elemento(3)
    lista.agregar_elemento(4)

    # Verifica que los elementos estén en el orden correcto
    actual = lista.cabeza
    valores_esperados = [1, 2, 3, 4]
    for valor in valores_esperados:
        assert actual.valor == valor
        actual = actual.siguiente

    # Agrega un elemento más y verifica que se añada al final
    lista.agregar_elemento(5)
    actual = lista.cabeza
    while actual.siguiente:
        actual = actual.siguiente
    assert actual.valor == 5


def test_agregar_elemento_otra_lista():
    # Crea otra instancia de ListaLigada
    otra_lista = ListaLigada()

    # Agrega elementos a la otra lista y verifica si la cabeza se establece correctamente
    otra_lista.agregar_elemento(10)
    assert otra_lista.cabeza.valor == 10

    otra_lista.agregar_elemento(20)
    assert otra_lista.cabeza.siguiente.valor == 20

    # Agrega más elementos y verifica que la lista se construya correctamente
    otra_lista.agregar_elemento(30)
    otra_lista.agregar_elemento(40)

    # Verifica que los elementos estén en el orden correcto
    actual = otra_lista.cabeza
    valores_esperados = [10, 20, 30, 40]
    for valor in valores_esperados:
        assert actual.valor == valor
        actual = actual.siguiente

    # Agrega un elemento más y verifica que se añada al final
    otra_lista.agregar_elemento(50)
    actual = otra_lista.cabeza
    while actual.siguiente:
        actual = actual.siguiente
    assert actual.valor == 50


def test_buscar_elemento():
    lista = ListaLigada()
    lista.agregar_elemento(1)
    lista.agregar_elemento(2)
    lista.agregar_elemento(2)
    assert lista.buscar_elemento(1)
    assert lista.buscar_elemento(2)
    assert not lista.buscar_elemento(3)


def test_elimina_cabeza():
    lista = ListaLigada()
    lista.agregar_elemento(1)
    lista.agregar_elemento(2)
    lista.elimina_cabeza()
    assert lista.cabeza.valor != 1
    assert lista.cabeza.siguiente is None


def test_elimina_cabeza2():
    # Caso 1: Eliminar la cabeza de una lista no vacía
    lista = ListaLigada()
    lista.agregar_elemento(1)
    lista.agregar_elemento(2)

    lista.elimina_cabeza()
    assert lista.cabeza.valor == 2
    assert lista.cabeza.siguiente is None

    # Caso 2: Intentar eliminar la cabeza de una lista vacía
    lista_vacia = ListaLigada()
    lista_vacia.elimina_cabeza()
    assert lista_vacia.cabeza is None


def test_elimina_rabo2():
    # Caso 1: Eliminar el rabo de una lista no vacía
    lista = ListaLigada()
    lista.agregar_elemento(1)
    lista.agregar_elemento(2)
    lista.agregar_elemento(3)

    lista.elimina_rabo()
    actual = lista.cabeza
    valores_esperados = [1, 2]
    for valor in valores_esperados:
        assert actual.valor == valor
        actual = actual.siguiente
    assert actual is None

    # Caso 2: Intentar eliminar el rabo de una lista con un solo elemento
    lista_un_elemento = ListaLigada()
    lista_un_elemento.agregar_elemento(10)
    lista_un_elemento.elimina_rabo()
    assert lista_un_elemento.cabeza is None

    # Caso 3: Intentar eliminar el rabo de una lista vacía
    lista_vacia = ListaLigada()
    lista_vacia.elimina_rabo()
    assert lista_vacia.cabeza is None


def test_elimina_rabo():
    lista = ListaLigada()
    lista.agregar_elemento(1)
    lista.agregar_elemento(2)
    lista.elimina_rabo()
    assert lista.cabeza.valor == 1
    assert lista.cabeza.siguiente is None


def test_tamagno():
    # Crea una instancia de ListaLigada
    lista = ListaLigada()

    # Verifica que el tamaño de una lista vacía sea 0
    assert lista.tamagno() == 0

    # Agrega elementos a la lista y verifica el tamaño
    lista.agregar_elemento(1)
    assert lista.tamagno() == 1

    lista.agregar_elemento(2)
    lista.agregar_elemento(3)
    assert lista.tamagno() == 3

    # Agrega más elementos y verifica el tamaño
    lista.agregar_elemento(4)
    lista.agregar_elemento(5)
    assert lista.tamagno() == 5

    # Verifica el tamaño de una lista vacía nuevamente
    lista_vacia = ListaLigada()
    assert lista_vacia.tamagno() == 0


def test_copia():
    # Crea una instancia de ListaLigada
    lista_original = ListaLigada()

    # Agrega elementos a la lista original
    lista_original.agregar_elemento(1)
    lista_original.agregar_elemento(2)
    lista_original.agregar_elemento(3)

    # Realiza una copia de la lista original
    lista_copiada = lista_original.copia()

    # Verifica que la copia no sea la misma instancia que la original
    assert lista_copiada is not lista_original

    # Verifica que las listas tengan el mismo tamaño
    assert lista_copiada.tamagno() == lista_original.tamagno()

    # Verifica que los elementos de la copia sean iguales a los de la original
    actual_original = lista_original.cabeza
    actual_copiada = lista_copiada.cabeza

    while actual_original:
        assert actual_copiada.valor == actual_original.valor
        actual_original = actual_original.siguiente
        actual_copiada = actual_copiada.siguiente


def test_str():
    # Crea una instancia de ListaLigada
    lista = ListaLigada()

    # Agrega elementos a la lista
    lista.agregar_elemento(1)
    lista.agregar_elemento(2)
    lista.agregar_elemento(3)

    # Verifica que la representación en cadena sea la esperada
    assert str(lista) == "1 -> 2 -> 3"

    # Agrega más elementos a la lista
    lista.agregar_elemento(4)
    lista.agregar_elemento(5)

    # Verifica la representación en cadena después de agregar más elementos
    assert str(lista) == "1 -> 2 -> 3 -> 4 -> 5"

    # Verifica la representación en cadena de una lista vacía
    lista_vacia = ListaLigada()
    assert str(lista_vacia) == ""
